import {useState, useEffect, useContext} from 'react'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import {Nav, Container, Row, Col, Button, Form, InputGroup} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import DataTable from 'react-data-table-component'



export default function Home(){
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const [data,setData] = useState([])
    const [tableRowsData, setTableRowsData] = useState(data);
    const getRecord = ()=>{
        fetch("http://localhost:5001/api/get_all_data.php", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json'
                },
                // body: JSON.stringify({
                //     data: 'B57U166'
                // })
            })
            .then(response => response.json())
            .then(result => {
                // console.log(result)
                setData(result)
                setTableRowsData(result)
            })

    }
    console.log(data)
    

  const onChange = async (e) => {
    var searchData = data.filter((item) => {
      if ((
        item.GOODS_CODE
          .toString()
          .toLowerCase()
          .includes(e.target.value.toLowerCase())
      )
      ||
      (
        item.PART_NUMBER
          .toString()
          .toLowerCase()
          .includes(e.target.value.toLowerCase())
      )
      ||
      (
        item.ITEM_CODE
          .toString()
          .toLowerCase()
          .includes(e.target.value.toLowerCase())
      ))
      {
        return item;
      }
    });
    setTableRowsData(searchData);
}

useEffect(() => {}, [tableRowsData]);

const columns = [
    {
        name: "Actions",
        button: true,
        cell: (row) => (
            <Button
                className="btn btn-outline btn-xs"
                onClick={(e) => handleButtonClick(e, row)}
            >
                Edit
            </Button>
        ),
    },
    {
        name: 'GOOS CODE',
        selector: row => row.GOODS_CODE,
    },
    {
        name: 'ITEM CODE',
        selector: row => row.ITEM_CODE,
    },
    {
        name: 'PART NUMBER',
        selector: row => row.PART_NUMBER,
    },
];

const handleButtonClick = (e, id) => {
    e.preventDefault();
    console.log("Row Id", id);
};

function MyComponent() {
    return (
        <DataTable
            id = 'data-table'
            pagination
            columns={columns}
            data={tableRowsData}
            selectableRows
            // expandableRows
            expandableRowsComponent={ExpandedComponent}
            
        />
    );
};
    return(
        <Container fluid className='pb-3' id="home-container" style={{height: '100%', width:'100%',  paddingTop: '90px',backgroundColor:'#3C5393', boxShadow: '3px 3px 10px #888888'}}>
            <Nav.Link className='link-hover' as={NavLink} to="/scanner">Scanner</Nav.Link>
            <div className='w-100 d-flex justify-content-center align-items-center'>
                <Button onClick={getRecord} variant="primary">Fetch Record</Button>
            </div>
            <div>
                {/* <br></br> */}
                <InputGroup className="mb-3" style={{ width: "20%" }}>
                <InputGroup.Text id="basic-addon1">Search</InputGroup.Text>
                    <Form.Control 
                        aria-describedby="basic-addon1"
                        type="text"
                        onChange={(e) => onChange(e)}  
                    />
                </InputGroup>
            </div>
            {MyComponent()}
        </Container>
    )
}