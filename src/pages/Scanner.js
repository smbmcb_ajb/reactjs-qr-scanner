import {useState, useEffect, useContext} from 'react'
// import Swal from 'sweetalert2'
// import {Link, NavLink} from 'react-router-dom'
// import {Nav, Container, Row, Col, Button, Form, InputGroup} from 'react-bootstrap'
// import {useNavigate, Navigate} from 'react-router-dom'
// import DataTable from 'react-data-table-component'
import { useMediaDevices } from "react-media-devices";
import { useZxing } from "react-zxing";


export default function Scanner(){  
  const [result, setResult] = useState("");
  const { ref } = useZxing({
    onResult(result) {
      setResult(result.getText());
    },
  });
    
return(
    <>
      <video ref={ref} />
      <p>
        <span>Last result:</span>
        <span>{result}</span>
      </p>
    </>
)
}